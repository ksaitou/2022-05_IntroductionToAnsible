---
title: Ansible でらくらくサーバー管理
paginate: true
---

# Ansible でらくらくサーバー管理

2022/05 by Kenji Saitou

![Slides are here](images/qrcode.png)

---

## 今日話す内容

- サーバの構成管理ツールの Ansible について以下をお話します
  - なんで必要？
  - どう使う？
  - ライブデモ
  - 利用実績その他
- 2H 程度

---

## 今日ターゲットにする人

- Ansible やったことない人・さわったことない人
- サーバ構成で忘れ物がある人
  - 複数台セットアップで抜け漏れマン 😅
- 自動化大好きマン
- Linux は普通程度には知ってる。SSH の知識はある。

**特に深く掘り下げず、入り口に立っていくことが目的です。**

---

# 我々は何に困っているのか？

---

## サーバの構成という退屈なタスク

――ある日あなたはサーバの運用を担当することになった

- 🐙 「`【検証環境用】サーバ構築手順書_20180101.docx`どうぞ。」
- 🐶 「了解。」
- 🐶 （一つ一つ TeraTerm 繋げて設定するの面倒だな…）

> - 手順 1. SCP で資材を `/home/foobar` にアップロードする。
> - 手順 2. Teraterm (TTLogMe) で SSH ログインする。
> - 手順 3. `cd /opt/foobar` をする。
>   ...
>
> 上記を検証環境上でやったのち、本番環境上では別途用意した手順書でやってください

---

## ところで私達、こんなにサーバ持ってます

![](images/we-have-servers.svg)

（＋複数のサブシステム・連携システム）

---

## そして間違えるのは誰のせいか？

- 🐶 「終わったよ」

...

- 🐙 「XXX を設定してないせいで本番のアプリが落ちたんだが…」
- 🐶 「**え゛っ**」

---

## 人間は機械ではない 〜 手順書をそのまま実行できますか？

- コマンドのコピペ、そして失敗。何がタイプミス？
- 実施した手順にチェックを押していくんですか？あ、今そこ飛ばさなかった？ちゃんとやった？
- 手順書がソフトウェアとして動くわけではない。

---

## 作業ってつまらない 〜 繰り返しが退屈ではありませんか？

- 何回も定期的に同じ手順を繰り返すの苦痛。
- ホスト数、何台あると思っている？
  - AP （増えたり減ったりする） / Batch / DB
- アプリ数、いくつあると思っている？
- 環境のバリエーション、いくつあると思っている？
- サブシステム、いくつあると思っている？

マトリクスで 30 超える数のデプロイが本番環境だけで必要というプロジェクトも多いのでは？

---

## 【結論】 人間が複数ホストを管理するのは不可能

---

# サーバ管理はどうあるべきか？

---

## 本番に変更を突っ込む前にちゃんと検証したい

- 結合 → 検証 → 本番 のようにフェーズごとに変更を検証したい
- 前のフェーズの環境に入れた変更が次の環境にも確実に入るにはどうすればいい？
  - 環境差分をうまくハンドリングしつつ全く同じ変更を環境ごとに入れたい

---

## サーバに個性を作りたくない

- **個性＝サーバをよく観察しないと分からない差異**
  - 引き継ぎやトラブル原因究明、サーバ復旧が大変になる
- アドホックに発生していくサーバ変更
  - 😸 「あ、 `pstree` / `iotop` が入ってない。入れたろ。」
  - 😸 「あ、 `XXXX` が入ってない。epel 追加して入れたろ。」
- 中途半端な作業
  - 😸 「`/etc/hosts` に今回新しく追加したホストを追加したろ」
  - 😸 「 サーバ A はこれ必要だけど、サーバ B は、まあ、…ええやろ 」

**この世から、かけがいのない大切なサーバを無くしたい**

いつでも減らせる、増やせる、作り直せる…… → [ペット vs 家畜](https://www.hava.io/blog/cattle-vs-pets-devops-explained)

---

## 進むべき方向

### ❌ NG ❌

- 🐶 「きちんと指差し確認をして、作業所にチェック印をして、作業漏れがないようにしよう！！」
- 🐶 「んじゃ、TeraTerm マクロ仕込んだろｗ」

### 💯 OK 💯

- 🐶 「サーバの **あるべき状態** をきちんと **コード** で管理しよう」

ようこそ！ IaC (Infrastructure as Code) の世界へ！

---

# Ansible とは何か？

---

![bg left:20% contain](images/Ansible_logo.svg)

## Ansible 沿革

- [公式サイト](https://www.ansible.com/) / [ドキュメント](https://docs.ansible.com/) / [ソースコード](https://github.com/ansible/ansible)
- 主にホスト（サーバ）管理に使用されている IaC ツール
- 開発: Red Hat (2012 年〜)
  - 商用サポート版あり
- Python で出来ている
  - 拡張も Python でできる

---

## どう使うの？

1. **Playbook （プレイブック）** というサーバのあるべき姿を書いたファイルを作成します。
   - 🐙 「サーバに `apache2` が入っていてほしいなあ」
2. 作成した Playbook を Ansible に実行させます。
   - 🐙 `$ ansible-playbook -i hosts playbook.yml`
3. Ansible が SSH (※) でサーバにつなぎながら Playbook に書かれている内容になるようにサーバを変更していきます。
   - 🤖: 1 回目「`apache2` が入ってないからインストールしよう」
   - 🤖: 2 回目「`apache2` は既に入っているからスキップ！」

---

## Playbook（プレイブック）とは

- サーバの **あるべき姿** を書いたもの
  - YAML (ヤムル) というフォーマットで書く
    - データを記述するので特にプログラミングは要らない
- プレイブックにかかれている内容になるように Ansible がサーバに変更作業を行う
  - 特定のパッケージが入っていてほしい
  - 特定のファイルが入っていてほしい
  - 特定のサービスを起動済みにしておいてほしい

---

## なぜ Playbook に 「あるべき姿」を書くの？

- Q. 手順書はもっと具体的に 「`$ apt install -y apache2`というコマンドを実行する」 といった手順を書くけど、なぜ Ansible では 「`apache2` が入っていてほしい」というような「あるべき姿」を書くのか？
  - A. [冪等](https://ja.wikipedia.org/wiki/%E5%86%AA%E7%AD%89)性（べきとうせい - Idempotency）を重視するから
- 冪等 ＝ 何度やっても同じ結果に収束する
- 一回 Playbook を用意したら、それをずっと流し続けても平気にしておく
  - ここが手順書と Ansible で決定的に違うところ
  - 新規サーバが入ってきても既存の Playbook を流せば同じ構成にできる
- IaC (Infrastructure as Code) ではほとんどのツールが「インフラのあるべき姿に収束する」＝「冪等性」に基づいて設計されています

---

## Ansible でできること、できないこと

- `ssh` （コマンドライン） 上でできることはだいたいできる
  - すごく自動化された SSH みたいな感じ
- 基本的には CLI （コマンドライン） 操作の自動化しかできない。
  - GUI 操作の自動化とかは無理。RPA ではない。

(※ Windows では SSH ではなく [WinRM](https://docs.ansible.com/ansible/latest/user_guide/windows_winrm.html) を使います)

---

## Ansible の[類似ツール](https://alternativeto.net/software/ansible/)

IaC は下記のように詳細に分類することがある

- IaC (Infrastructure as Code) ＝ サーバやクラウドインフラを生やす
  - Terraform, Pulumi, AWS CFn/CDK, AzureRM 等
- CaC (Configuration as Code) ＝ サーバ(OS)に構成を積む
  - [Chef](https://www.chef.io/): 元祖サーバ構成管理ツールといえばこれ
  - [Puppet](https://puppet.com/)
  - **Ansible (今回)**: 後発であり他ツールの反省が生かされている

---

## Ansible の利点

- **一番の利点は人気があること**
- サーバ側にエージェントプログラムの常駐が不要 (エージェントレス)
  - 一番の利点。誰でも始めやすい。
  - サーバが SSH で入れて、Python が入っていれば OK。
- プログラミング出てこない
  - ほぼ YAML で記述。書き方が統一されやすい。
  - ノンプログラマーでもとっつきやすい。
  - 反面、自由度はあまりない。
- シンプル
  - 色々な仕組みはあるものの、最終的には上から下に SSH 上で操作をしていくスタイルに近い。

---

## Ansible 向いてる

- 大量のホストを一括管理したい
  - 同じ構成のアプリケーションサーバが数台ある場合など効果的
- サーバのセットアップをコードとして文書化したい

ターミナル上で出来ていた作業を自動化するには向いています。

---

## Ansible 向いてない

- やたら凝ったセットアップが必要
  - 「FB チャネルでネットワークストレージをボリュームに追加して再起動云々」 みたいな手順は管理できないと思う
- ログインを自動化できない
  - 本番環境がすごいオフライン　パスワード数種類
  - いちいちパスワード入力が必要なケース

---

# Ansible をはじめる<br />〜準備編

---

## Ansible で使用される言語

- 特にプログラミング言語は出てきません
- 主に YAML によるツリー構造のデータ記述と規約に沿ったファイル群のみ

```
hosts
site.yml
roles/
  nginx/
    defaults/
      main.yml
    tasks/
      main.yml
    templates/
      nginx.conf.j2
```

慣れればかんたん

---

## Ansible で使用される言語 - YAML (`*.yml` or `*.yaml`)

- Ansible の構成ファイル・設定値など全てこの形式が使われます。
  - Kubernetes や docker-compose など最近のツールは採用していることが多い
- 木構造のデータを書く言語
  - JSON とほぼ等価だと思えばいいです。 (JSON よりは記述力は上)
  - 実は JSON をそのまま書いても YAML としても通用します。
- [いい入門記事があるので](http://magazine.rubyist.net/?0009-YAML)、これを読めば OK。

```yaml
- name: install nginx
  apt:
    name:
      - nginx
    state: present
```

---

## Ansible で使用される言語 - [Jinja2](http://jinja.pocoo.org/) (`*.j2`)

- Jinja = Python 用のテンプレートエンジン。 2 = version 2
- 設定ファイルのテンプレート（↓）に使うほか、YAML の中に `"{{ variable_name }}"` のように記述して使うことが多い

```yaml
logging: # こういうYAMLの設定値と ↓ のJinja2テンプレートと組み合わせることが多い
  enabled: true
  host: "127.0.0.1"
  port: 20050
```

```jinja
{% if logging.enabled -%}
[OUTPUT]
  Name forward
  Host {{ logging.host }}
  Port {{ logging.port }}
{% else %}
[OUTPUT]
  Name null
{% endif %}
```

---

## Ansible の中身を読むなら YAML と Jinja がわかればいい

書いていればそのうち慣れる

---

## かんたん YAML マスター

### リテラル

```yaml
string # 文字列
"string" # '文字列' でも可
null # NULL
123,456,789 # 数値。小数も可能。
true # 真偽値: true(on) / false(off)
2018-04-20 # 日付
```

```yaml
|
  複数行の
  文字列も
  書ける（各行の先頭インデントは必要だが、全てトリムされる）
```

---

## かんたん YAML マスター

### リスト

```yaml
# ブロックスタイル
- listItem01
- listItem02
---
# フロースタイル (JSONと同じ書き方)
[listItem01, listItem02]
```

### マップ

```yaml
# ブロックスタイル
mapKey1: mapValue1
mapKey2: mapValue2
---
# フロースタイル (JSONと同じ書き方)
{ mapKey1: mapValue1, mapKey2: mapValue2 }
```

---

## かんたん YAML マスター

### リストとマップの複合

```yaml
- mapKey1: mapValue1
  mapKey2: mapValue2
- { mapKey1: mapValue1, mapKey2: mapValue2 }
```

```yaml
services:
  nginx:
    image: "nginx"
```

---

## かんたん YAML マスター

### ドキュメント

ドキュメント ＝ 1 つの YAML ツリーデータ。XML でいえば、ルート要素にあたる。

YAML ではこれを `---` で区切ることにより複数持つことができる。

```yaml
# 以下は3ドキュメント構成。 --- が区切り。
---
- list1
- list2
---
"私は元気です"
---
mapKey1: mapValue2
```

※ 特に Ansible では複数ドキュメントは出てきません

---

## かんたん YAML マスター

### エイリアス定義＆参照

```yaml
- &foobar "こんにちは！"
- "またまた、明日"
- *foobar
# => ["こんにちは！", "またまた、明日", "こんにちは！"]
```

---

## YAML を知っておくといいこと

- 割りと色々な場面で使われていて知識として潰しは効く
  - docker-compose / Kubernetes / CI の定義 (GitHub Actions / GitLab CI/CD / CircleCI) / CloudFormation (AWS) 等
- 普及率
  - あの日輝いていた XML は死んだ
  - JSON は人が手書きするフォーマットではない
- 最近人気の競合フォーマットとして [TOML](https://github.com/toml-lang/toml) がある

Ansible を使う上では、結局 Ansible が YAML を読んで何をどうやって解釈しているか理解できていたほうがスムーズ（エラーの特定などしやすい）

---

### かんたん [Jinja2](http://jinja.pocoo.org/docs/2.10/templates/) マスター

```yaml
"{% ... %}" # ステートメント
"{{ ... }}" # 式
"{# ... #}" # コメント
```

- いろいろあるけど、だいたい YAML 中や設定のテンプレートファイル中で `"{{ variable_name }}"` みたいなのしか使わない
- 必要に応じてググって書ければ十分

---

## Ansible の動作環境

- [macOS / Linux / 一部の UNIX](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) で動く
- **Windows はサポートされていない** (※)  
  → VM を建てるか WSL (Windows Subsystem for Linux) 使いましょう
  - WSL1: VM 上の Windows の場合
    - 結構無茶苦茶な仕組みだが、ちゃんと動く
  - VM/WSL2: ベアメタル上の Windows の場合
  - VSCode と組み合わせると楽

```console
# 1. OS標準のパッケージでいれる
$ sudo apt install ansible
# 2. もしくは Python の pip でいれる
$ sudo pip install ansible
```

※ Ansible で Windows を管理することは可能だが、Ansible を生の Windows で動かすのは無理

---

# Ansible をはじめる<br />〜 シンプルスタート編

---

## シンプルスタート

かなり前置きが長くなってしまいましたが、まずはシンプルに Ansible を使い始めてみましょう。

---

## マシン準備

遊び相手として [AlmaLinux](https://almalinux.org/ja/) \* 2 を [Vagrant](https://www.vagrantup.com/) ([VirtualBox](https://www.virtualbox.org/))で用意します。

```console
$ cd examples/simple_start
$ vagrant up
```

上記コマンドで、下記サーバが立ち上がり、 `vagrant` というユーザで公開鍵認証によりログインできるようになります。

1. `webserver`: `172.16.100.2`
2. `dbserver`: `172.16.100.3`

---

## 単純に単発のモジュールを実行させてみる

下記のファイルの設定を確認しましょう。

```
hosts ansible.cfg
```

コマンドを流してみましょう。

```console
# 応答チェック (pingモジュール)
$ ansible -m ping web_server

# コマンドの実行 (commandモジュール)
$ ansible -m command -a "whoami" web_server

# rootに昇格して実行
$ ansible -m command -b -a "whoami" web_server

# yum で何か入れてみる (yumモジュール)
$ ansible -m yum -b -a "name=uuid state=present" web_server
$ ansible -m command -a "uuid" web_server
```

---

## サーバの構成管理をしてみる

`playbook.yml` など を確認しましょう。

```yaml
# 構成はプレイブックというYAMLファイルに記述していく。
- hosts: all
  become: true
  tasks:
    # Fiwewalld を有効化
    - name: enable firewalld
      systemd:
        name: firewalld
        enabled: yes
        state: started
```

---

## サーバの構成管理をしてみる

プレイブックを流すコマンドを流してみましょう。

```console
$ ansible-playbook playbook.yml
# もう1回!
$ ansible-playbook playbook.yml
```

Web ページが立ち上がっているはずです。

http://172.16.100.2/my_php_app/

---

## シンプル編 まとめ

- Ansible で扱うエンティティ
  - [モジュール](https://docs.ansible.com/ansible/latest/collections/all_plugins.html) = `yum` とか
  - インベントリ = `hosts`
    - 物理的なサーバホストの記述とグルーピングやらはここに書く
  - プレイブック = `playbook.yml`
- Ansible 実行をスムーズにするためには...
  - Ansible 用ユーザを用意しておく
    - 公開鍵で入れるようにしておく
    - sudo で昇格できるようにしておく

---

# Ansible をはじめる<br />〜やや複雑編

---

## 🐸 やや複雑編

もう少し多いサーバ群を構成してみます。

- ステージング (stg)
  - Web \* 1
  - DB \* 1
- 本番 (prd)
  - Web \* 4
  - DB \* 2

立ち上げは前回と同様に行います。 （8 台なので時間がかかります）

```console
$ cd examples/complicated_start
$ vagrant up
```

---

## 🐸 大合唱

複数台のマシンで一気にモジュールを実行してみましょう。

```console
# ステージング 2台 に一気に実行
$ ansible -i inventories/staging/hosts -m ping all

# 本番 6台 に一気に実行
$ ansible -i inventories/production/hosts -m ping all
```

---

## 🐸 環境ごとのプレイブック反映

環境ごとにプレイブックを反映させていきましょう。

（通常は結合 → ステージング → 本番の順に構成の適用が問題ないか試験しつつスライドすることになります。）

```console
# ステージング 2台
$ ansible-playbook -i inventories/staging/hosts site.yml

# 本番 6台 に一気に実行
$ ansible-playbook -i inventories/production/hosts site.yml
```

- ステージング: http://172.16.101.10/
- 本番: http://172.16.102.10/ http://172.16.102.11/ http://172.16.102.12/ http://172.16.102.13/

---

# One More Ansible

---

## 私と Ansible

- Q. どれぐらい Ansible 使ってる？
  - A. かれこれ 2018 年から 4 年以上使っている。ほぼほとんどのプロジェクトで使ってるし、開発環境も Ansible で構成している。
- Q. 他のツールに浮気した？
  - A. 一切しなかった。結局 Ansible がエージェントレスで一番使いやすい。
- Q. アプリのデプロイや配布はどうやってる？
  - A. 基本的にアプリは Docker + docker-compose で構成。Docker イメージタグをつけてレジストリにおいて、それぞれのサーバから pull している。要するにアプリはバージョン番号がついたパッケージとして管理してる。
- Q. Ansible のプレイブックの設計のコツは？
  - A. YAML がたくさんできるのは諦めよう。ディレクトリは[ベストプラクティス](http://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)に沿おう。ロールは細かくわけよう。[Ansible Lint](https://ansible-lint.readthedocs.io/en/latest/) を入れよう。

---

## こんなこともできるよ Ansible

- Windows サーバの管理 (WinRM)
- Ansible を自作プラグインで拡張 (Python)
- ルーター・スイッチ・ロードバランサーの管理 (Cisco IOS、BIG-IP など)
- Kubernetes クラスタの管理
- VMware (ESXi) の管理
- 他の人が作った Ansible プレイブックを利用する - [Ansible Galaxy](https://galaxy.ansible.com/)
- Ansible の実行管理プラットフォーム - [Ansible Tower](https://docs.ansible.com/ansible-tower/)
- インフラの試験、インフラ管理コマンドの実行
- Jenkins と組み合わせる / リモートからファイルを集める etc.

---

## どうやって自分達のサーバに Ansible を使うか？

- 必要なホスト（サーバ）を書き出す
- グループ（ホストの種類）と、それぞれのグループに持たせている機能（ロール）を設計する
- 環境間(本番・ステージング・結合等)の差異を設計する

- Q. 既に動いているサーバに適用可能か？
  - A. 手腕次第。ただ、よく書き換えるところ（例:アプリのリリース部）だけ Ansible 管理にするのはアリ。

---

# さいごに

---

## まとめ

- Ansible はシンプルでわかりやすい
- エージェントレスなので始める際の障壁が少ない
- 身近なところから始めたらどうだろうか？
  - 開発環境
  - 本番サーバの初期セットアップ
  - 新規案件

---

## 学びたい人向けへの資源

- [Ansible 実践ガイド](https://www.amazon.co.jp/dp/B01NAH7NAA/)
  - 普通に知りたいことは網羅されている。これさえあれば大丈夫。
- [Ansible クックブック](https://www.amazon.co.jp/dp/4295013544/)
  - 最近(2022/03)出た本。やや Ansible になれてる人ならこれもいい気はするが、正直 Linux より後ろの章(VMware〜コンテナ〜ネットワーク機器〜SDN)は大多数に需要がなさそう。Windows サーバをカバーしてほしかった。

---

## 資料はこちら

- ソースコード、資料  
  https://gitlab.com/ksaitou/2022-05_IntroductionToAnsible
- スライド  
  https://ksaitou.gitlab.io/2022-05_IntroductionToAnsible/

---

# 勉強会後に出た質問への回答

---

### Ansible で躓きやすいポイントを教えてほしい 〜 1 / 3

ベストプラクティス([1](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html) / [2](https://docs.ansible.com/ansible/latest/user_guide/sample_setup.html) / [日本語](https://docs.ansible.com/ansible/2.9_ja/user_guide/playbooks_best_practices.html))に書いてある内容に従えばある程度の罠は避けられるかなあと思います。

ディレクトリの切り方なんかは [Alternative Directory Layout](https://docs.ansible.com/ansible/latest/user_guide/sample_setup.html#alternative-directory-layout) のほうがわかりやすいので私は推しています。

私は導入初期時、↑ のルールを無視してそのままダラーッとプレイブック etc を作って失敗しました。ゼロから作成しなおしました。
（きちんとホストグループ・ロールは作成しましょう。）

---

### Ansible で躓きやすいポイントを教えてほしい 〜 2 / 3

あとは、プレイブックはきちんとよく適用するもの・一度適用すればほとんど再適用がいらないもので切り分けることも必要になることが多いかなと思います。
もちろん全部入りのプレイブックを毎変更ごとに適用するほうが望ましいですが、現実的にはあまりパフォーマンスもよくないし、影響範囲も広がってくるので、ある程度細切れのプレイブック＋それらをまとめた全部入りプレイブック ( site.yml )にわけて実行することが多いと思います。

---

### Ansible で躓きやすいポイントを教えてほしい 〜 3 / 3

それと、Ansible はソースコードリポジトリに入れるので、秘密の扱いはちゃんと学習しておいたほうがいいです。
（e.g., データベースのパスワードなど作業には必要ではあるものの、公にしたくないもの）
現実的には Ansible Vault を使うことになります。暗号化に用いる秘密鍵のみ、リポジトリに保存しないで、どこかインフラ関係者のみがアクセスできるストレージにおいておきましょう。

---

## Ansible は Ubuntu 22.04 (Jammy Jellyfish) でインストール可能か？

可能です。

下記の Vagrant で検証しましたが、特に問題なくインストール完了しました。

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/jammy64"
  config.vm.provision "shell", inline: "apt-get update && apt-get install -y ansible"
end
```

`ansible --version` によると、 `ansible 2.10.8` が入っているようです。

---

## Ansible のサーバに構成を反映するメカニズムについて知りたい 〜 1 / 2

Ansible 自体はインベントリにある各サーバにつないで、そのサーバに適用すべきプレイブックに書かれているモジュールを実行しているだけに過ぎません。
なので、サーバ構築のメインは Ansible のコア部にあるわけではなく、Ansible が用意している[莫大なモジュール](https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html)の中にあります。（自作も可能です）

たとえば、Debian 系の Linux でパッケージをインストールする [`apt` というモジュール](https://docs.ansible.com/ansible-core/devel/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module)についていえば、

```yaml
apt:
  name: nginx
  state: present
```

---

## Ansible のサーバに構成を反映するメカニズムについて知りたい 〜 2 / 2

こういった書き方で、apt モジュールが起動して、apt モジュールがうまい具合に `nginx` パッケージについてインストール済み (`present`) の状態に保ってくれるという仕組みです。（実際には SSH でつないで、単純にパッケージがインストールされているかチェックしてなければインストールするなどの泥臭いスクリプトを作成して流しているだけです）

ただ、結局手で作業する以上の魔法を Ansible が持っているわけではなく、Ansible が扱う対象（Linux など）についてプレイブックを書く側を無知でいさせてくれるわけではありません。

---

## Chef の名前の由来について

Chef については [Wikipedia](https://en.wikipedia.org/wiki/Progress_Chef) によると当初マリオネットという名前だったけれども、長ったらしいのでレシピブック（Chef におけるプレイブック）からシェフととったようです。注文（レシピ）どおりといえば、そういう感じですね。

---

# おわり
