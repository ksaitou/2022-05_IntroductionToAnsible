# 2022 年版 Ansible 入門 〜 Ansible でらくらくサーバ管理

## スライドはここで見ることができます

https://ksaitou.gitlab.io/2022-05_IntroductionToAnsible/

## スライドの作り方

### スライドの開発

```
# 依存性取得
$ yarn

# サーバ: http://localhost:8080
$ yarn start

# HTMLで出力
$ yarn build
```

### 参考

- marp (プレゼンツール) - https://github.com/marp-team/marp-cli
  - https://marpit.marp.app/markdown
